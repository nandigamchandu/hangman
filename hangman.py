"""Hangman quiz."""

from collections import defaultdict
import re
import random as rn


def check_word_length(data):
    """Check given word length is present or not."""
    word_lenths = data.keys()
    min_word, max_word = (min(word_lenths), max(word_lenths))
    word_length = input(f'Select word length between {min_word}-{max_word} = ')
    if re.findall(r'\d', word_length):
        if int(word_length) not in data.keys():
            print('Sorry try another word length')
            word_length = check_word_length(data)
    else:
        print('Give input in numbers.')
        word_length = check_word_length(data)
    return int(word_length)


def check_category(option):
    """Check give category."""
    category = input("Select above category options = ")
    if re.findall(r'\d', category):
        if category not in option.keys():
            print('Your selection is not in options')
            path = check_category(option)
        else:
            path = option[category]
    else:
        print('Try numbers as options')
        path = check_category(option)
    return path


def read_input():
    """Read file, inputs and return word."""
    # asking for category selection
    print('Categories \n1) Sport Names \n2) Color names\
    \n3) Indian Cricketers names \n4) Telugu Movie names')

    option = ({'1': 'sports.txt', '2': 'colors.txt',
               '3': 'cricketersnames.txt', '4': 'telugumovies.txt'})

    # check given option is present in given category
    path = check_category(option)

    # Read file
    with open(path) as f:
        data = f.read()

    # Counting word length
    quiz = {i: len(re.findall(r'[\w\d]', i))
            for i in map(str.strip, data.splitlines())}
    quiz_words = defaultdict(list)
    # grouping same length words
    for key, value in quiz.items():
        quiz_words[value].append(key)

    # check given word length present or not
    word_length = check_word_length(quiz_words)

    words_lst = quiz_words[word_length]

    # selecting random word of given length
    word_idx = rn.randint(0, len(words_lst)-1)
    word = words_lst[word_idx]
    return word


# %%
def input_cha(word, ch, right_lst, wrong_lst, chances):
    """Check input character present in word or not."""
    match = re.findall(ch, word, flags=re.I)
    if match and ch not in right_lst:
        # it add guessed char to rigth guess list if it is not present in it
        right_lst.append(ch)
    elif ch not in wrong_lst and not match:
        # it add guessed char to wrong guess list if it is not present in it
        wrong_lst.append(ch)
        chances -= 1
    return right_lst, wrong_lst, chances


def filling_word(word, lst):
    """Display guess word."""
    lst = ''.join(lst)
    print('Your guesses = ', end='')
    for i in word:
        if re.findall(i, lst, flags=re.I):
            print(i, end=' ')
        else:
            print('_', end=' ')
    print('    ', end='')


def wrong_word(wrong_list):
    """Display wrong attempts."""
    print('Worng attempts = ', end='')
    for i in wrong_list:
        print(i, end=' ')
    print('    ', end='')


# %%
def main():
    """Call func."""
    play_again = 1
    while play_again == 1:
        # read inputs, files and return word
        word = read_input().lower()
        # seven chances are given
        chances = 7
        # for right guess
        right_lst = list()
        right_lst = right_lst + re.findall(r'[\.\-\s\:]', word)
        right_lst = list(set(right_lst))
        # for wrong guess
        wrong_lst = list()
        while chances > 0:
            char1 = input("Enter your guess letter = ")

            # checking input char is present in word or not
            right_lst, wrong_lst, chances = (input_cha(word, char1, right_lst,
                                                       wrong_lst, chances))
            # display wrong letters
            wrong_word(wrong_lst)
            # display right letters
            filling_word(word, right_lst)

            print(f'Chances left: {chances} \n')
            if len(right_lst) >= len(set(word)):
                print('you won the game')
                break
        if chances == 0:
            print(f'your are out of chances, word = {word}')
        print('Do you want to play again')
        play_again = int(input('Enter 1 to play/ 0 to exit: '))


main()
